class T
{
	float v;
	public: T(float f) { v = f; }
};

T foo(char t1);

void bar()
{
	char c = 'a';
	T result = foo(c);
}