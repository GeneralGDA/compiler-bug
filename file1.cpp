class T;

// commenting this removes the bug
T foo(char c);

class T
{
	float v;
	public: T(float f) { v = f; }
};

T foo(char)
{
	return T(6);
}
